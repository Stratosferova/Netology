class Animal:
    lifestyle = 'ferma'
    name = None
    height = 0
    weight = 0
    feed = 0
    water = 0


    def __init__(self, height, weight, name):
        self.height = height
        self.weight = weight
        self.name = name
        self.water = round((height * 3), 2)
        self.feed = round((weight / 7), 2)
        self.stroll = height * 1000

    def print_info(self):
        print('Название животного: {}, Вода: {} л, Еда: {} кг, Сколько нужно гулять: {} метров'.format(self.name, self.water, self.feed, self.stroll))


goat_object = Animal(0.5, 25, 'коза')
cow_object = Animal(2, 100, 'корова')
sheep_object = Animal(1, 60, 'овца')
gus_object = Animal( 0.5, 4.5, 'гусь')
chicken_object = Animal(0.3, 2.5, 'курица')
duck_object = Animal(0.4, 3.5, 'утка')

goat_object.print_info()
cow_object.print_info()
sheep_object.print_info()
gus_object.print_info()
chicken_object.print_info()
duck_object.print_info()


