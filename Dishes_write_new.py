
# coding: utf-8

# In[1]:

def get_cook_book(filename):
    cook_book = {}
    with open(filename, encoding ='utf-8') as file:
        for line in file:
            dish = line.strip().lower()
            cnt_ingr = file.readline().strip()
            cook_book[dish] = []
            for i in range(int(cnt_ingr)):
                ingr = file.readline().strip().split(' | ')
                cook_book[dish].append({'ingridient_name':ingr[0],'quantity':int(ingr[1]),'measure':ingr[2]})
            file.readline()
    return cook_book

def get_shop_list_by_dishes(cook_book, dishes, person_count):
    shop_list = {}
    for dish in dishes:
        for ingridient in cook_book[dish]:
            new_shop_list_item = dict(ingridient)
            new_shop_list_item['quantity']*= person_count
            shop_list[new_shop_list_item['ingridient_name']] = new_shop_list_item
            if new_shop_list_item['ingridient_name'] not in shop_list: #чтобы сложить дубли продуктов
                shop_list[new_shop_list_item['ingridient_name']] - new_shop_list_item
            else:
                shop_list[new_shop_list_item['ingridient_name']]['quantity'] += new_shop_list_item['quantity']
    return shop_list

def print_shop_list(shop_list):        
    for shop_list_item in shop_list.values():
        print('{ingridient_name},{quantity},{measure}'.format(**shop_list_item)) #args

def create_shop_list(cook_book):
    person_count = int(input('Введите количество человек: '))
    dishes = input('Введите блюда в расчете на одного человека (через запятую): ').lower().split(',')
    shop_list = get_shop_list_by_dishes(cook_book, dishes, person_count)
    print_shop_list(shop_list)
    
cook_book = get_cook_book('recipe_book.txt')
create_shop_list(cook_book)


# In[ ]:




# In[ ]:



