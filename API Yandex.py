import requests

TOKEN = 'AQAAAAAFhsXNAASOLCtldCbfg0lRifOYKBbve5Y'
class Ya_Metrika:
    url = 'https://api-metrika.yandex.ru/stat/v1/data'

    def __init__(self, token):
        self.token = token

    def get_data(self, counter_id):
        params = {
            'metrics': 'ym:s:visits,ym:s:pageviews,ym:s:users',
            'id': counter_id,
            'oauth_token': self.token
        }
        respons = requests.get(self.url, params)
        return (respons.json()['totals'])

metrika = Ya_Metrika(TOKEN)
print(metrika.get_data(46051017))