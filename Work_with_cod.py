
# coding: utf-8

# In[1]:

import json
import chardet
import re
from collections import Counter


# In[13]:

with open('Documents/News/newscy.json', 'rb') as f: #опеределяем кодировку
    news = f.read()
    result = chardet.detect(news)
    print(result)


# In[14]:

with open ('Documents/News/newscy.json', 'r', encoding='KOI8-R') as file: #открываем в нужной кодировке
    news_set = json.load(file)
fact = str(news_set)


# In[15]:

def all_words(): #функуция, которая указываетоткуда имено мы берем тексты (чтобы избежать лишних слов)
    all_words = []
    for item in news_set['rss']['channel']['items']:
        all_words.append(item['title'])
        all_words.append(item['description'])
    all_words = ' '.join(all_words) #собираем все части в текст
    split_words = re.split(' ', all_words) #разбиваем по словам
    return split_words

def find_long_words(split_words): #функция фильтрует слова, которые содержать более 6 букв и выводит 10 самых часто используемых
    word_6 = []
    for word in split_words:
        if (len(word) > 6):
            word_6.append(word)
    words_count = Counter(word_6).most_common(10)
    return words_count

words_count = find_long_words(all_words()) 
print(words_count)


# In[16]:

with open('words.txt', "a")  as file: #не придумала ничего лучше - я дозаписывала результаты в файл после каждого прогона(
    print('10 самых популярных слов в newscy.json:\n', words_count,  file=file) 


# In[25]:

all_lists = open('words.txt','r')
lines = all_lists.readlines()
lines


# In[ ]:



