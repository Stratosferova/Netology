import requests

API_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
API_KEY = 'trnsl.1.1.20170910T194441Z.8336c7c41f7dae9a.8b8701f38af73eaa9b32d215f7a8872be82fcc45'


def read_files(filename):
    with open(filename, 'rb') as translation:
        data = translation.read()
        data_read = data.decode()
    return(data_read)


def translate_en_ru(text):
    response = requests.post(
        API_URL,
        params=dict(
            key=API_KEY,
            lang='ru'
        ),
        data=dict(
            text=text
        )
    )
    return(response.json()['text'])

news_set = ['DE.txt', 'ES.txt', 'FR.txt']
for new_set in news_set:
    print(translate_en_ru(read_files(new_set)))
