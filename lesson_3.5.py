
import osa

#Задача №1
def temperature(Temperature, FromUnit, ToUnit):
    dict_unit = { 'F' : 'degreeFahrenheit', 'C' : 'degreeCelsius' }
    FromUnit = dict_unit[FromUnit]
    ToUnit = dict_unit[ToUnit]
    client = osa.Client('http://www.webservicex.net/ConvertTemperature.asmx?WSDL')
    return client.service.ConvertTemp(Temperature, FromUnit, ToUnit)

sum_temp = 0
cnt_temp = 0
with open('temps.txt') as file:
    for line in file:
        file_str = line.strip().split()
        sum_temp += (temperature(file_str[0], file_str[1], 'C'))
        cnt_temp += 1
    print(sum_temp / cnt_temp)


#Задача №2
def exchange(amount, fromCurrency, toCurrency):
    client = osa.Client('http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL')
    return client.service.ConvertToNum('', fromCurrency, toCurrency, amount, True)

sum_flights = 0
with open('currencies.txt') as file:
    for line in file:
        file_str = line.strip().split()
        sum_flights += int(exchange(file_str[1], file_str[2], 'RUB'))
    print(sum_flights)


#Задача №3
def path_to_miles(LengthValue, fromLengthUnit, toLengthUnit):
    dict_unit = { 'mi': 'Miles' }
    fromLengthUnit = dict_unit[fromLengthUnit]
    client = osa.Client('http://www.webservicex.net/length.asmx?WSDL')
    return client.service.ChangeLengthUnit(LengthValue, fromLengthUnit, toLengthUnit)

sum_paths = 0
with open('travel.txt') as file:
    for line in file:
        file_str = line.strip().split()
        sum_paths += path_to_miles(file_str[1].replace(',', ''), file_str[2], 'Kilometers')
    print(sum_paths)








