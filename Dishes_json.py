
# coding: utf-8

# In[72]:

import json
import xml.etree.cElementTree as ET


# In[73]:

cook_book = {
      'яичница': [
        {'ingridient_name': 'яйца', 'quantity': 2, 'measure': 'шт.'},
        {'ingridient_name': 'помидоры', 'quantity': 100, 'measure': 'гр.'}
        ],
      'стейк': [
        {'ingridient_name': 'говядина', 'quantity': 300, 'measure': 'гр.'},
        {'ingridient_name': 'специи', 'quantity': 5, 'measure': 'гр.'},
        {'ingridient_name': 'масло', 'quantity': 10, 'measure': 'мл.'}
        ],
      'салат': [
        {'ingridient_name': 'помидоры', 'quantity': 100, 'measure': 'гр.'},
        {'ingridient_name': 'огурцы', 'quantity': 100, 'measure': 'гр.'},
        {'ingridient_name': 'масло', 'quantity': 100, 'measure': 'мл.'},
        {'ingridient_name': 'лук', 'quantity': 1, 'measure': 'шт.'}
        ]
  }


# In[74]:

with open('cook_book.json', 'w', encoding = 'utf-8') as cooking:
    json.dump(cook_book, cooking, indent=2) #сохранить список рецептов в json


# Дополним код - сделаем так, чтобы он читал json, а не работал со словарем

# In[51]:

def get_all_reipe():
    with open('recipe_book.json') as file:
        recipe = json.load(file)
    
def get_shop_list_by_dishes(dishes, person_count):
    shop_list = {}
    for dish in dishes:
        for ingridient in recipe[dish]:
            new_shop_list_item = dict(ingridient)
            new_shop_list_item['quantity']*= person_count
            shop_list[new_shop_list_item['ingridient_name']] = new_shop_list_item
            if new_shop_list_item['ingridient_name'] not in shop_list: #чтобы сложить дубли продуктов
                shop_list[new_shop_list_item['ingridient_name']] - new_shop_list_item
            else:
                shop_list[new_shop_list_item['ingridient_name']]['quantity'] += new_shop_list_item['quantity']
    return shop_list

def print_shop_list(shop_list):        
    for shop_list_item in shop_list.values():
        print('{ingridient_name},{quantity},{measure}'.format(**shop_list_item)) #args

def create_shop_list():
    person_count = int(input('Введите количество человек: '))
    dishes = input('Введите блюда в расчете на одного человека (через запятую): ').lower().split(',')
    shop_list = get_shop_list_by_dishes(dishes, person_count)
    print_shop_list(shop_list)
create_shop_list()




# In[22]:

print(recipe) #да, действительно торт в json есть))


# Попробуем сделать xml

# In[69]:

root = ET.Element("cook_book")
doc = ET.SubElement(root, "dish")
ET.SubElement(doc, "ingridient_name").text = "яйца"
ET.SubElement(doc, "quantity",).text = "2"
tree = ET.ElementTree(root)
tree.write("cooking.xml")


# In[71]:

tree2 = ET.parse("cooking.xml")
for i in tree2.iter():
    print(i)


# In[ ]:




# In[ ]:



