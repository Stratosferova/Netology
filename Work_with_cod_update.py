# coding: utf-8

import json
import chardet
import re
from collections import Counter


def read_document(filename):
    with open(filename, 'rb') as f:
        data = f.read()
        chardet_file = chardet.detect(data)
        decoded_string = data.decode(chardet_file['encoding'])
        news_set = json.loads(decoded_string)
    return news_set


def all_words(news_set):
    my_words = []
    for item in news_set['rss']['channel']['items']:
        my_words.append(item['title'])
        my_words.append(item['description'])
    my_words = ' '.join(my_words)
    split_words = re.split(' ', my_words)
    return split_words


def find_long_words(split_words):
    word_6 = []
    for word in split_words:
        if len(word) > 6:
            word_6.append(word)
    words_count = Counter(word_6).most_common(10)
    return words_count


def reading_result():
    news_loads = ['newsafr.json', 'newscy.json',
                  'newsfr.json', 'newsit.json']
    for news_load in news_loads:
        news_get = read_document(news_load)
        words_count = find_long_words(all_words(news_get))
        print(news_load)
        print(words_count)


reading_result()
