import json
import chardet
import re
from collections import Counter

with open('newscy.json', 'rb') as f:
    news = f.read()
    result = chardet.detect(news)
    print(result)
with open('newscy.json', 'r', encoding='KOI8-R') as file:
    news_set = json.load(file)
fact = str(news_set)


def all_words():
    global split_words
    all_words = []
    for item in news_set['rss']['channel']['items']:
        all_words.append(item['title'])
    all_words.append(item['description'])
    all_words = ' '.join(all_words)
    split_words = re.split(' ', all_words)
    return split_words


def find_long_words(split_words):
    word_6 = []
    for word in split_words:
        if len(word) > 6:
            word_6.append(word)
    words_count = Counter(word_6).most_common(10)
    return words_count


words_count = find_long_words(all_words())
print(words_count)

with open('words.txt', "a") as file:
    print('10 самых популярных слов в newscy.json:\n', words_count)

all_lists = open('words.txt', 'r')
lines = all_lists.readlines()
