import requests

API_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
API_KEY = 'trnsl.1.1.20170910T194441Z.8336c7c41f7dae9a.8b8701f38af73eaa9b32d215f7a8872be82fcc45'


def translate_en_ru(srcfile, dstfile, srclang, dstlang='ru'):
    with open(srcfile, 'rb') as f:
        data = f.read()
        text = data.decode()
    response = requests.post(
        API_URL,
        params=dict(
            key=API_KEY,
            lang='-'.join([srclang, dstlang])
        ),
        data=dict(
            text=text
        )
    )
    with open(dstfile, 'w') as f:
        f.write(' '.join(response.json().get('text', [])))

translate_en_ru('DE.txt', 'DE-RU.txt', 'de')
translate_en_ru('ES.txt', 'ES-RU.txt', 'es')
translate_en_ru('FR.txt', 'FR-RU.txt', 'fr')
